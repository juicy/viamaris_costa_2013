// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.
map_initialize = function( dom_id, lplaces )
{
	//return;
	var siteOptions = {
		zoom: 4,
		center: new google.maps.LatLng(43.6158,13.5187),
		disableDefaultUI: true,
		//mapTypeId: google.maps.MapTypeId.ROADMAP
		mapTypeId: google.maps.MapTypeId.TERRAIN,
		mapTypeControl: false,
		navigationControl: false,
		navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL}
	}
// 	if(map == null)
	var map = new google.maps.Map(document.getElementById(dom_id), siteOptions);
	if( lplaces.length > 0 ) {
		set_places(map, lplaces);
	}

	function set_places(map, locations) {
		var latlngbounds = new google.maps.LatLngBounds();
		var image = new google.maps.MarkerImage(SiteBaseUrl+'img/map-marker.png', new google.maps.Size(24, 41) );
		var shd = new google.maps.MarkerImage(SiteBaseUrl+'img/map-marker-shd.png',
			new google.maps.Size(32, 25),
			new google.maps.Point(0,0),
			new google.maps.Point(2,20)
		);
		var dg = new Array;
		var dg2 = new Array;
		var f;
		for(var i=0; (i<100); i++){
			if(i < 10){
				dg[i] = new google.maps.MarkerImage(SiteBaseUrl+'img/digits/' + i + '.png',
					new google.maps.Size(7,11),
					new google.maps.Point(0,0),
					new google.maps.Point(4,36)
				);
			}
			else{
				f=Math.floor((i/10));
				dg[i] = new google.maps.MarkerImage(SiteBaseUrl+'img/digits/' + f + '.png',
					new google.maps.Size(7,11),
					new google.maps.Point(0,0),
					new google.maps.Point(7,36)
				);

				dg2[i] = new google.maps.MarkerImage(SiteBaseUrl+'img/digits/' + (i%10) + '.png',
					new google.maps.Size(7,11),
					new google.maps.Point(0,0),
					new google.maps.Point(1,36)
				);
			}
		}

		for (var i = 0; i < lplaces.length; i++){
			var myLatLng = new google.maps.LatLng(locations[i][2], locations[i][3]);
			latlngbounds.extend(myLatLng);
			var marker = new google.maps.Marker({
				position: myLatLng,
				map: map,
				icon: image,
				shadow: shd,
				title: locations[i][0],
				color: 'black',
				zIndex: 10
			});
			if(locations[i][1] < 10){
				var dmarker = new google.maps.Marker({
					position: myLatLng,
					map: map,
					icon: dg[locations[i][1]],
					title: locations[i][0],
					color: 'black',
					zIndex: 11
				});
			}
			else if(locations[i][1] < 100){
				var dmarker = new google.maps.Marker({
					position: myLatLng,
					map: map,
					icon: dg[locations[i][1]],
					title: locations[i][0],
					color: 'black',
					zIndex: 11
				});
				var dmarker2 = new google.maps.Marker({
					position: myLatLng,
					map: map,
					icon: dg2[locations[i][1]],
					title: locations[i][0],
					color: 'black',
					zIndex: 11
				});
			}
		}
		map.setCenter( latlngbounds.getCenter(), map.fitBounds(latlngbounds));
	};
}