$(document).ready(() ->
  tabs('.tabs')
  popups()
  $('.fancybox').fancybox()
  show_hide()
  $("a[href^='http://']").attr("target","_blank");
)

show_hide = () ->
  $('.show_hide').click ->
    $(this).css('display', 'none').parents('section').find('.hide').removeClass('hide')
    false

tabs = (boxs) ->
  $(boxs).each ->
    box = this
    lbls = $(box).find('.label')
    $(lbls).click ->
      to_active_box = $(box).find($(this).find('a').attr('href'))
      $(box).find('.tb').removeClass('active')
      $(to_active_box).addClass('active')
      $(lbls).removeClass('active')
      $(this).addClass('active')
      false


popups = () ->
  dp = '[data-role="popup"]'
  pops = $(dp)
  pops_open = $('[data-role="popup_open"]')
  pops_close = $('[data-role="popup_close"]')
  wind = '[data-role="popup_window"]'

  $(document).keyup((e) ->
    if e.keyCode == 27 && $(".open#{dp}").length > 0
      close(".open#{dp}")
  )

  open = (pop) ->
    window.location.hash = pop.replace('#', '')
    $(pop).css('display', 'block')
    $(pop).addClass('open')
    $('body').css('overflow', 'hidden')
    if $(pop).find('.map').length > 0
      id = $(pop).find('.map').attr('id')
      if id == 'map-canvas'
        map_initialize('map-canvas', places)
      else
        map_initialize('map-canvas1', places1)

  close = (pop) ->
    history.pushState('', document.title, window.location.pathname);
    $(pop).css('display', 'none')
    $(pop).removeClass('open')
    $('body').css('overflow', 'auto')

  $('body').click ->
    if $(".open#{dp}").length > 0
      close(".open#{dp}")

  $(pops).each ->

    id = $(this).attr('id')

    hash = window.location.hash.replace('#', '')
    if hash != '' && hash == id
      open("##{id}")

    $(this).find(wind).click((e) ->
      e.stopPropagation();
    )

  $(pops_open).each ->
    $(this).click ->
      target = $(this).attr('data-target')
      if $(target).length > 0
        open(target)
      false

  $(pops_close).each ->
    $(this).click ->
      target = $(this).parents(dp)
      if $(target).length > 0
        close(target)
      false